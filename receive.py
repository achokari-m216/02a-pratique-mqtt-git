#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#----------------------------------------------------------------------------
#           EPTM - Ecole professionnelle technique et des métiers
#
# Nom du fichier source              : receive.py
#
# Auteur (Nom, Prénom)               : Jérémy Michaud
# Classe                             : MQTT_receive
# Module                             : M216
# Date de création                   : 30.01.2023
#
# Description succincte du programme :
#    Reçois des paquets MQTT selon les paramètres globaux
#----------------------------------------------------------------------------

import random
import os

from paho.mqtt import client as mqtt_client


broker = 'mqtt-eptm.jcloud.ik-server.com'
port = 11521
topic = "message/"
max_qos = 2

# generate client ID with pub prefix randomly
client_id = f'python-mqtt-{random.randint(0, 100)}'
username = ''
password = ''
connected = False
messages = ""

def show_prompt():
    print("\r                                                         ", end="", flush= True)

def connect_mqtt() -> mqtt_client:
    def on_connect(client, userdata, flags, rc, properties):
        if rc == 0:
            global messages
            messages += "\nConnected to MQTT Broker!"
            global connected
            connected = True
        else:
            print("Failed to connect, return code %d\n", rc)

    client = mqtt_client.Client(mqtt_client.CallbackAPIVersion.VERSION2, client_id)
    client.username_pw_set(username, password)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client


def subscribe(client: mqtt_client, topic):
    def on_message(client, userdata, msg):
        global messages
        message = msg.payload.decode()
        messages += f"\n{message}"
        os.system("cls")
        print(messages)
        show_prompt()

    client.subscribe("topic", max_qos)
    client.on_message = on_message


def run():
    client = connect_mqtt()
    global topic
    topic = "CHANGE_ME_PLEASE"
    inputValue = input("Veuillez entrer votre topic : ")
    subscribe(client, topic)
    client.loop_forever()


if __name__ == '__main__':
    run()
