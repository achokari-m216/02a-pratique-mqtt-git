#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#----------------------------------------------------------------------------
#           EPTM - Ecole professionnelle technique et des métiers
#
# Nom du fichier source              : send.py
#
# Auteur (Nom, Prénom)               : Jérémy Michaud
# Classe                             : MQTT_send
# Module                             : M216
# Date de création                   : 06.03.2024
#
# Description succincte du programme :
#   Envoie des paquets MQTT selon les paramètres globaux
#----------------------------------------------------------------------------

import random
import time

from paho.mqtt import client as mqtt_client


broker = 'mqtt-eptm.jcloud.ik-server.com'
port = 11521
topic = "kariacho/examen"
retain=False
qos=0

# generate client ID with pub prefix randomly
client_id = f'python-mqtt-{random.randint(0, 1000)}'
username = ''
password = ''

def connect_mqtt():
    def on_connect(client, userdata, flags, rc, properties):
        if rc != 0:
            print("Failed to connect, return code %d\n", rc)

    client = mqtt_client.Client(mqtt_client.CallbackAPIVersion.VERSION2, client_id)
    client.username_pw_set(username, password)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client


def publish(client, msg):
    result = client.publish(topic, msg, qos, retain)
    status = result[0]
    if status == 0:
        print("Envoi du message réussi")
    else:
        print(f"Failed to send message to topic {topic}")


def run():
    client = connect_mqtt()
    client.loop_start()
    publish(client, "Bonjour")
    while True :
        inputValue = input("Veuillez entrer votre message : ")
        client.publish(client,inputValue)


if __name__ == '__main__':
    run()